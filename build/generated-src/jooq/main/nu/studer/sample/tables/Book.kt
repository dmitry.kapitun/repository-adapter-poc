/*
 * This file is generated by jOOQ.
 */
package nu.studer.sample.tables


import nu.studer.sample.Test
import nu.studer.sample.tables.records.BookRecord

import org.jooq.Field
import org.jooq.ForeignKey
import org.jooq.Name
import org.jooq.Record
import org.jooq.Row3
import org.jooq.Schema
import org.jooq.Table
import org.jooq.TableField
import org.jooq.TableOptions
import org.jooq.impl.DSL
import org.jooq.impl.Internal
import org.jooq.impl.SQLDataType
import org.jooq.impl.TableImpl


/**
 * This class is generated by jOOQ.
 */
@Suppress("UNCHECKED_CAST")
open class Book(
    alias: Name,
    child: Table<out Record>?,
    path: ForeignKey<out Record, BookRecord>?,
    aliased: Table<BookRecord>?,
    parameters: Array<Field<*>?>?
): TableImpl<BookRecord>(
    alias,
    Test.TEST,
    child,
    path,
    aliased,
    parameters,
    DSL.comment(""),
    TableOptions.table()
) {
    companion object {

        /**
         * The reference instance of <code>test.book</code>
         */
        val BOOK: Book = Book()
    }

    /**
     * The class holding records for this type
     */
    override fun getRecordType(): Class<BookRecord> = BookRecord::class.java

    /**
     * The column <code>test.book.id</code>.
     */
    val ID: TableField<BookRecord, String?> = createField(DSL.name("id"), SQLDataType.VARCHAR(36), this, "")

    /**
     * The column <code>test.book.name</code>.
     */
    val NAME: TableField<BookRecord, String?> = createField(DSL.name("name"), SQLDataType.VARCHAR(255), this, "")

    /**
     * The column <code>test.book.author</code>.
     */
    val AUTHOR: TableField<BookRecord, String?> = createField(DSL.name("author"), SQLDataType.VARCHAR(36), this, "")

    private constructor(alias: Name, aliased: Table<BookRecord>?): this(alias, null, null, aliased, null)
    private constructor(alias: Name, aliased: Table<BookRecord>?, parameters: Array<Field<*>?>?): this(alias, null, null, aliased, parameters)

    /**
     * Create an aliased <code>test.book</code> table reference
     */
    constructor(alias: String): this(DSL.name(alias))

    /**
     * Create an aliased <code>test.book</code> table reference
     */
    constructor(alias: Name): this(alias, null)

    /**
     * Create a <code>test.book</code> table reference
     */
    constructor(): this(DSL.name("book"), null)

    constructor(child: Table<out Record>, key: ForeignKey<out Record, BookRecord>): this(Internal.createPathAlias(child, key), child, key, BOOK, null)
    override fun getSchema(): Schema? = if (aliased()) null else Test.TEST
    override fun `as`(alias: String): Book = Book(DSL.name(alias), this)
    override fun `as`(alias: Name): Book = Book(alias, this)

    /**
     * Rename this table
     */
    override fun rename(name: String): Book = Book(DSL.name(name), null)

    /**
     * Rename this table
     */
    override fun rename(name: Name): Book = Book(name, null)

    // -------------------------------------------------------------------------
    // Row3 type methods
    // -------------------------------------------------------------------------
    override fun fieldsRow(): Row3<String?, String?, String?> = super.fieldsRow() as Row3<String?, String?, String?>
}
