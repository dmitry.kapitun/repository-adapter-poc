import examples.Application
import org.jooq.tools.jdbc.MockConfiguration
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.DynamicPropertyRegistry
import org.springframework.test.context.DynamicPropertySource
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.testcontainers.containers.MySQLContainer
import org.testcontainers.shaded.com.fasterxml.jackson.databind.ObjectMapper

@ExtendWith(SpringExtension::class)
@SpringBootTest(classes = [Application::class, MockConfiguration::class])
@ActiveProfiles("functional-test")
@Tag("functional")
abstract class FunctionalTest {

    @Autowired
    lateinit var objectMapper: ObjectMapper

    companion object {
        private val container = MySQLContainer("mysql:8.0.28")
            .withDatabaseName("assets")
            .withUsername("test")
            .withPassword("test")
            .withReuse(true)

        @BeforeAll
        @JvmStatic
        fun init() {
            container.start()
        }

        @DynamicPropertySource
        @JvmStatic
        fun registerDynamicProperties(registry: DynamicPropertyRegistry) {
            registry.add("spring.datasource.url", container::getJdbcUrl)
            registry.add("spring.datasource.username", container::getUsername)
            registry.add("spring.datasource.password", container::getPassword)
        }
    }
}