import examples.jooq.AuthorJooqRepository
import examples.jpa.AuthorJpaRepository
import model.Author
import model.Id
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired

class AuthorRepositoryTest: FunctionalTest() {

    @Autowired
    lateinit var jooqRepo: AuthorJooqRepository

    @Autowired
    lateinit var jpaRepo: AuthorJpaRepository

    @Test
    fun `to-from model should work`() {
        val author = Author(Id.randomId(), "some author")

        assertThat(author)
            .isEqualTo(jooqRepo.toModel(jooqRepo.toRecord(author)))
            .isEqualTo(jpaRepo.toModel(jpaRepo.toRecord(author)))
    }

    @Test
    fun `insert should work`() {
        val author1 = Author(Id.randomId(), "jooq author")
        val author2 = Author(Id.randomId(), "jpa author")

        val result1 = jooqRepo.insert(author1)
        val result2 = jpaRepo.insert(author2)

        val queried1 = jooqRepo.get(author1.id)
        val queried2 = jpaRepo.get(author2.id)

        assertThat(author1)
            .isEqualTo(result1)
            .isEqualTo(queried1)

        assertThat(author2)
            .isEqualTo(result2)
            .isEqualTo(queried2)
    }

    @Test
    fun `update should work`() {
        val author1 = jooqRepo.insert(Author(Id.randomId(), "jooq author"))
        val author2 = jpaRepo.insert(Author(Id.randomId(), "jpa author"))

        val updated1 = author1.copy(name = "some author")
        val updated2 = author2.copy(name = "other author")

        val result1 = jooqRepo.update(updated1)
        val result2 = jpaRepo.update(updated2)

        val queried1 = jooqRepo.get(author1.id)
        val queried2 = jpaRepo.get(author2.id)

        assertThat(updated1)
            .isEqualTo(result1)
            .isEqualTo(queried1)

        assertThat(updated2)
            .isEqualTo(result2)
            .isEqualTo(queried2)
    }

    @Test
    fun `delete should work`() {
        val author1 = jooqRepo.insert(Author(Id.randomId(), "jooq author"))
        val author2 = jpaRepo.insert(Author(Id.randomId(), "jpa author"))

        assertThat(jooqRepo.find(author1.id)).contains(author1)
        assertThat(jpaRepo.find(author2.id)).contains(author2)

        jooqRepo.delete(author1)
        jpaRepo.delete(author2)

        assertThat(jooqRepo.find(author1.id)).isEmpty
        assertThat(jpaRepo.find(author2.id)).isEmpty
    }

}