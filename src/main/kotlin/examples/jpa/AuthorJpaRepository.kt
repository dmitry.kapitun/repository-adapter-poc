package examples.jpa

import java.util.UUID
import model.Author
import model.Id
import org.springframework.beans.factory.annotation.Autowired
import repository.JpaRepository
import repository.jpa.records.AuthorRecord

class AuthorJpaRepository(@Autowired private val authorRepo: AuthorRepository): JpaRepository<Author, AuthorRecord>(authorRepo) {
    override fun toModel(record: AuthorRecord): Author {
        return Author(Id.fromUUID(record.id!!), record.name!!)
    }

    override fun toRecord(model: Author): AuthorRecord {
        val record = AuthorRecord()
        record.id = model.id.value
        record.name = model.name
        return record
    }
}

interface AuthorRepository: org.springframework.data.jpa.repository.JpaRepository<AuthorRecord, UUID>