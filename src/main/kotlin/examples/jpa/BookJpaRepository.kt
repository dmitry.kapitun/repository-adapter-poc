package examples.jpa

import java.util.UUID
import model.Book
import model.Id
import org.springframework.beans.factory.annotation.Autowired
import repository.JpaRepository
import repository.jpa.records.BookRecord

class BookJpaRepository(@Autowired private val bookRepo: BookRepository): JpaRepository<Book, BookRecord>(bookRepo) {
    override fun toModel(record: BookRecord): Book {
        return Book(Id.fromUUID(record.id!!), record.name!!, Id.fromUUID(record.author!!))
    }

    override fun toRecord(model: Book): BookRecord {
        val record = BookRecord()
        record.id = model.id.value
        record.name = model.name
        record.author = model.author.value
        return record
    }
}

interface BookRepository: org.springframework.data.jpa.repository.JpaRepository<BookRecord, UUID>