package examples.jooq

import java.util.UUID
import model.Author
import model.Id
import nu.studer.sample.tables.records.AuthorRecord
import nu.studer.sample.tables.references.AUTHOR
import org.jooq.DSLContext
import org.springframework.beans.factory.annotation.Autowired
import repository.JooqRepository

class AuthorJooqRepository(@Autowired dsl: DSLContext) :
    JooqRepository<Author, AuthorRecord, nu.studer.sample.tables.Author>(dsl, AUTHOR, AUTHOR.ID) {

    override fun toModel(record: AuthorRecord): Author {
        return Author(Id.fromUUID(UUID.fromString(record.id!!)), record.name!!)
    }

    override fun toRecord(model: Author): AuthorRecord {
        val record = AuthorRecord()
        record.id = model.id.value.toString()
        record.name = model.name
        return record
    }
}