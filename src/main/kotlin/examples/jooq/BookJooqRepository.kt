package examples.jooq

import java.util.UUID
import model.Book
import model.Id
import nu.studer.sample.tables.records.BookRecord
import nu.studer.sample.tables.references.BOOK
import org.jooq.DSLContext
import org.springframework.beans.factory.annotation.Autowired
import repository.JooqRepository

class BookJooqRepository(@Autowired dsl: DSLContext) :
    JooqRepository<Book, BookRecord, nu.studer.sample.tables.Book>(dsl, BOOK, BOOK.ID) {

    override fun toModel(record: BookRecord): Book {
        return Book(Id.fromUUID(UUID.fromString(record.id!!)), record.name!!, Id.fromUUID(UUID.fromString(record.author!!)))
    }

    override fun toRecord(model: Book): BookRecord {
        val record = BookRecord()
        record.id = model.id.value.toString()
        record.name = model.name
        record.author = model.author.value.toString()

        return record
    }
}