package model

data class Author(override val id: Id<Author>, val name: String) : BaseModel<Author>(id) {
}