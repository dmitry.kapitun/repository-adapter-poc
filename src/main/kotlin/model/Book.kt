package model

data class Book(override val id: Id<Book>, val name: String, val author: Id<Author>): BaseModel<Book>(id) {
}