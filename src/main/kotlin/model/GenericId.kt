package model

abstract class GenericId<E: BaseModel<E>, T>(val value: T, val modelClass: Class<E>) {

}