package model

import java.util.UUID
import java.util.UUID.randomUUID

class Id<E: BaseModel<E>>(value: UUID, modelClass: Class<E>) : GenericId<E, UUID>(value, modelClass) {

    companion object {

        inline fun <reified E: BaseModel<E>>fromUUID(id: UUID): Id<E> {
            return Id(id, E::class.java)
        }

        inline fun <reified E : BaseModel<E>>randomId(): Id<E> {
            return fromUUID(randomUUID())
        }
    }
}