package repository

import model.BaseModel
import model.Id

interface WriteOnlyRepository<E: BaseModel<E>> {

    fun insert(model: E): E

    fun update(model: E): E

    fun delete(model: E) {
        delete(model.id)
    }

    fun delete(id: Id<E>)

}