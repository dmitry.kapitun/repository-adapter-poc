package repository

import java.util.Optional
import model.BaseModel
import model.Id

interface ReadOnlyRepository<E: BaseModel<E>> {

    fun find(id: Id<E>): Optional<E>

    fun get(id: Id<E>): E {
        return find(id).orElseThrow { RuntimeException("${id.modelClass.simpleName}[${id.value}] not found") }
    }

    fun findAll(): Collection<E>

}