package repository

import model.BaseModel

interface Repository<E: BaseModel<E>, R>: WriteOnlyRepository<E>, ReadOnlyRepository<E> {

    fun toModel(record: R): E

    fun toRecord(model: E): R

}