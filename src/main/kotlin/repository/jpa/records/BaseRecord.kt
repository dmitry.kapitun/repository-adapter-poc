package repository.jpa.records

import java.util.UUID
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id

@Entity
abstract class BaseRecord {
    @Id
    @Column(name = "id", nullable = false)
    open var id: UUID? = null

}