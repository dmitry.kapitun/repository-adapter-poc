package repository.jpa.records

import java.util.UUID

class BookRecord: BaseRecord() {

    var name: String? = null

    var author: UUID? = null

}