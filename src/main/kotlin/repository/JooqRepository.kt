package repository

import java.util.Optional
import model.BaseModel
import model.Id
import nu.studer.sample.tables.records.AuthorRecord
import org.jooq.DSLContext
import org.jooq.Record
import org.jooq.Table
import org.jooq.TableField

abstract class JooqRepository<E: BaseModel<E>, R: Record, T: Table<R>>(protected val dsl: DSLContext, protected val table: T, protected val idField: TableField<R, String?>): Repository<E, R> {

    override fun find(id: Id<E>): Optional<E> {
        return Optional.ofNullable(dsl.selectFrom(table).fetchOne { toModel(it) })
    }

    override fun findAll(): Collection<E> {
        return dsl.selectFrom(table).fetch { toModel(it) }
    }

    override fun insert(model: E): E {
        dsl.insertInto(table).set(toRecord(model)).execute()
        return get(model.id)
    }

    override fun update(model: E): E {
        dsl.update(table).set(toRecord(model)).execute()
        return get(model.id)
    }

    override fun delete(id: Id<E>) {
        dsl.deleteFrom(table).where(idField.eq(id.value.toString())).execute()
    }
}