package repository

import java.util.Optional
import java.util.UUID
import model.BaseModel
import model.Id
import org.springframework.data.jpa.repository.JpaRepository
import repository.jpa.records.BaseRecord

abstract class JpaRepository<E: BaseModel<E>, R: BaseRecord>(protected val jpaRepo: JpaRepository<R, UUID>): Repository<E, R> {

    override fun find(id: Id<E>): Optional<E> {
        return jpaRepo.findById(id.value).map { toModel(it) }
    }

    override fun findAll(): Collection<E> {
        return jpaRepo.findAll().map { toModel(it) }
    }

    override fun insert(model: E): E {
        return toModel(jpaRepo.save(toRecord(model)))
    }

    override fun update(model: E): E {
        return toModel(jpaRepo.save(toRecord(model)))
    }

    override fun delete(id: Id<E>) {
        return jpaRepo.deleteById(id.value)
    }
}