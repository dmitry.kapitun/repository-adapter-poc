import org.jooq.meta.jaxb.ForcedType
import org.jooq.meta.jaxb.Logging
import org.jooq.meta.jaxb.Property

plugins {
    id("org.springframework.boot") version "2.7.3"
    id("io.spring.dependency-management") version "1.0.13.RELEASE"
    id("nu.studer.jooq") version "7.1.1"
//    id("org.liquibase.gradle") version "2.1.0"
    kotlin("plugin.jpa") version "1.7.0"
    kotlin("jvm") version "1.7.10"
    kotlin("plugin.spring") version "1.7.0"
}

group = "org.example"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

buildscript {
    dependencies {
        classpath("mysql:mysql-connector-java:8.0.30")
        classpath("org.jooq:jooq-codegen:3.16.4")
//        classpath("org.liquibase:liquibase-core:4.16.0")
//        classpath("org.liquibase:liquibase-gradle-plugin:2.1.0")
    }
}

dependencies {
    implementation("org.jetbrains.kotlin:kotlin-stdlib")

//    implementation("org.liquibase:liquibase-core:4.16.0")
    implementation("mysql:mysql-connector-java:8.0.30")

    implementation("jakarta.xml.bind:jakarta.xml.bind-api:3.0.1")

    implementation("org.jooq:jooq:3.16.4")
    implementation("org.jooq:jooq-meta:3.16.4")
    implementation("org.jooq:jooq-codegen:3.16.4")

    implementation("org.springframework.boot:spring-boot-starter-data-jpa")
    implementation("org.springframework.boot:spring-boot-starter-jooq")
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    runtimeOnly("com.h2database:h2")

//    liquibaseRuntime("mysql:mysql-connector-java:8.0.30")
//    liquibaseRuntime("org.liquibase:liquibase-core:4.16.0")
//    liquibaseRuntime("info.picocli:picocli:4.6.1")
//    liquibaseRuntime("org.yaml:snakeyaml:1.30")

    jooqGenerator("mysql:mysql-connector-java:8.0.30")

    testImplementation("org.springframework.boot:spring-boot-starter-test")
    testImplementation("org.testcontainers:junit-jupiter:1.16.3")
    testImplementation("org.testcontainers:mysql:1.16.3")

}

tasks.getByName<Test>("test") {
    useJUnitPlatform()
}

//liquibase {
//    activities.register("main") {
//        this.arguments = mapOf(
//            "logLevel" to "warn",
//            "searchPath" to "src/main/resources/",
//            "changeLogFile" to "src/main/resources/db/changelog/db.changelog-master.xml",
//            "url" to "jdbc:mysql://localhost:9001/test",
//            "referenceUrl" to "jdbc:mysql://localhost:9001/test",
//            "username" to "test",
//            "password" to "test",
//            "driver" to "com.mysql.cj.jdbc.Driver"
//        )
//    }
//    runList = "main"
//}

jooq {
    version.set("3.16.4")
    configurations {
        create("main") {  // name of the jOOQ configuration
            generateSchemaSourceOnCompilation.set(true)  // default (can be omitted)

            jooqConfiguration.apply {
                logging = Logging.WARN
                jdbc.apply {
                    driver = "com.mysql.cj.jdbc.Driver"
                    url = "jdbc:mysql://localhost:9001/test"
                    user = "test"
                    password = "test"
                    properties.add(Property().apply {
                        key = "ssl"
                        value = "true"
                    })
                }
                generator.apply {
                    name = "org.jooq.codegen.KotlinGenerator"
                    database.apply {
                        name = "org.jooq.meta.mysql.MySQLDatabase"
                        inputSchema = "test"
                    }
                    generate.apply {
                        isDeprecated = false
                        isRecords = true
                        isImmutablePojos = true
                        isFluentSetters = true
                    }
                    target.apply {
                        packageName = "nu.studer.sample"
                        directory = "build/generated-src/jooq/main"  // default (can be omitted)
                    }
                    strategy.name = "org.jooq.codegen.DefaultGeneratorStrategy"
                }
            }
        }
    }
}
